import sys
import json
import jsonschema
from jsonschema import validate


with open(sys.argv[2], 'r') as schema:
	schemaFile = json.load(schema)


def validateJson(jsonFile):
    try:
        validate(instance=jsonFile, schema=schemaFile)
    except jsonschema.exceptions.ValidationError as err:
        print(err.message)
        print(err.absolute_path)
        print(err.instance)
        return False
    except Exception as ex:
        print(f'Other exception -> {str(ex)}')
        return False
    return True


# Convert json to python object.
with open(sys.argv[1], 'r') as jsonData:
    jsonFile = json.load(jsonData)


# validate it
isValid = validateJson(jsonFile)
if isValid:
    print("Given JSON data is Valid")
else:
    print("Given JSON data is InValid")